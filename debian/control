Source: variety
Section: graphics
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3,
               python3-distutils-extra,
Maintainer: James Lu <jlu@debian.org>
Standards-Version: 4.6.0
Rules-Requires-Root: no
Homepage: http://peterlevi.com/variety/
Vcs-Git: https://salsa.debian.org/jlu-guest/variety.git
Vcs-Browser: https://salsa.debian.org/jlu-guest/variety

Package: variety
Architecture: all
Depends: gir1.2-gdkpixbuf-2.0,
         gir1.2-gexiv2-0.10,
         gir1.2-glib-2.0,
         gir1.2-gtk-3.0,
         gir1.2-notify-0.7,
         gir1.2-pango-1.0,
         imagemagick,
         python3-bs4,
         python3-lxml,
         python3-cairo,
         python3-configobj,
         python3-dbus,
         python3-gi,
         python3-gi-cairo,
         python3-packaging,
         python3-pil,
         python3-requests,
         ${misc:Depends},
         ${python3:Depends}
Recommends: gir1.2-ayatanaappindicator3-0.1 | gir1.2-appindicator3-0.1,
            python3-httplib2,
            fortune-mod,
            libavif-gdk-pixbuf
Suggests: feh | nitrogen,
          gnome-shell-extension-appindicator
Description: Wallpaper changer, downloader and manager
 Variety is a wallpaper manager for Linux systems. It supports numerous
 desktops and wallpaper sources, including local files and online
 services: Flickr, Wallhaven, Unsplash, and more.
 .
 Where supported, Variety sits as a tray icon to allow easy pausing and
 resuming. Otherwise, its desktop entry menu provides a similar set of
 options.
 .
 You can also install the following (suggested) packages to enhance Variety's
 functionality on certain desktop setups:
  * feh | nitrogen: used for wallpaper changing on i3, openbox, and dwm
  * gnome-shell-extension-appindicator | gnome-shell-extension-top-icons-plus:
    adds tray icon / indicator support to GNOME Shell, so that Variety's tray
    icon can be displayed
